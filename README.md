### About

This is a scoring server developed for the Red Team vs. Blue Team Exercise (called Heist) as part of Wargames 2019. This takes a lot of inspiration from CTFd but also has a lot of features customized for the format of the event.

### Usage

#### Deploying the application.

You could deploy it with docker with the following commands:

`cd backend`

`docker build -t scoring-server .`

`docker run -p 8082:8082 -t scoring-server`

Or you could run it directly.

1. First install `pipenv` with `pip install pipenv`

2. Install dependences and start a shell with: `pipenv install && pipenv shell`

3. Then simply run the `docker-entrypoint.sh` file and you should have the app running at port 8082. NOTE: This will probably be updated later so that migrations aren't run every time.

Note: you should intialize the service with `python manage.py intialize_services` if they are not intialized already.

Also: Important note: When deploying the Wordpress, run this command on the "wordpress" database in mysql:

`update wp_options set option_value='http://192.168.43.151/wordpress' where option_name='siteurl' or option_name='home';`

Otherwise wordpress will not work properly.

#### Using the application

When there is no DB, the application will redirect you to the "Create Admin" page and start all the services that are monitored. 

Once there, non-logged in users can see the Scoreboard, which is at this time, public. If you go into teams, you should see the Blue Team with 5000 points already assigned. Points are assigned based on "Scoring Events". Scoring Events are anything that can give or take points from a team.

The different types of scoring events are: 

* FLAG submission

* Uptime Scoring

Flags act as proof of compromise. Uniquely identifiable flags should be posted in the various services and systems that the scoring server will monitor. If the Red Team gains access to those flags, say for example in the users table in some DB, then they can post it here to prove that they have access to that service.

Another way the Red Team can score is by taking down services. Based on how it's configured, every time a service is reported "down" i.e. is inaccessible by the scoring server, a certain number of points will be deducted from the blue team.

Some of these points can be recovered when a service is brought back up. This depends on the following formula:

```

total_loss = total number of points lost.

amount_of_service_down = Amount of times the service was reported as down. (i.e the Overall amount, not just the last time.)

points returned = (total_loss / amount of service down) * recovery multiplier.

```

Teams and team members can be added from the Teams page.

### Configuration

Different configuration options are available in the config file. See `config.py` for details


### Things to note

The feature set for this app is kept as minimal as possible. This means that a lot of the features that seem "missing" will in fact not be added since that would take a lot of time that we do not have. They may be added in future updates.

Originally, data dumps as a scoring mechanism were also planned, however, since submitting flags can prove that you have access to sensitive data, this feature is currently dropped.

At this time this app does not have rate limiting. 

Currently it uses sqlite3 although this is probably going to change in the future.


### Also, angular frontend (proposed feature)

To build the angular frontend (not currently used).

`ng build --prod --base-href="./" --output-path="../scoring_server/scoring_server/static/" --output-hashing=none --watch`

