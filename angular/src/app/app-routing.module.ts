import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FlagScoringComponent } from './components/flag-scoring/flag-scoring.component';
import { ScoreViewComponent } from './components/score-view/score-view.component';

const routes: Routes = [
	{ path: '', component: ScoreViewComponent },
	{ path: 'flags', component: FlagScoringComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
