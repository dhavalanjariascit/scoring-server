import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FlagScoringComponent } from './components/flag-scoring/flag-scoring.component';
import { ScoreViewComponent } from './components/score-view/score-view.component';

@NgModule({
  declarations: [
    AppComponent,
    FlagScoringComponent,
    ScoreViewComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
