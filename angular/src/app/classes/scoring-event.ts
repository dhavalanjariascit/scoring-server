export class ScoringEvent {
	public constructor(
		public EventId: any,
		public Timestamp: any,
		public team: any,
		public event_type: any,
		public description: any,
		public point_value: any
	)
	{ }
}
