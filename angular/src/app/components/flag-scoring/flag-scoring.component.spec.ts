import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlagScoringComponent } from './flag-scoring.component';

describe('FlagScoringComponent', () => {
  let component: FlagScoringComponent;
  let fixture: ComponentFixture<FlagScoringComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlagScoringComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlagScoringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
