import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/core/http';

@Injectable({
  providedIn: 'root'
})
export class ScoreServiceService {

  scoreUrl = 'http://localhost:8000/api/getScore';

  constructor(private http: HttpClient) { }

  public getAllScores(){
  	return this.http.get(this.scoreUrl);

  }
}
