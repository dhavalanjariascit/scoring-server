from uptime_scoring.tools import initialize_services
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Intialize services based on config"

    def handle(self, *args, **options):
        initialize_services()
