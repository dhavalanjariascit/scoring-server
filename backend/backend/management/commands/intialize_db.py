from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User, Group, Permission
import logging
log = logging.getLogger('app')

class Command(BaseCommand):
    help = "Intialize the DB with an Admin group, an admin user and its permissions"

    def handle(self, *args, **options):
        admin_group, created = Group.objects.get_or_create(name='admins')
        admin_permission = Permission.objects.create(
            codename='can_admin',
            name='Can Admin',
        )
        admin_group.permissions.add(admin_permission)
        admin_user = User.objects.get_or_create(name='dhaval')
