"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin, auth
from django.urls import path, include
from django.contrib.auth.views import LoginView

from test_endpoint.views import *
from user_management import views as user_views
from . import settings, views

from django.conf.urls.static import static


urlpatterns = [
    path('admin/', admin.site.urls),
]

urlpatterns += [
    path('', user_views.index, name='index')
]

urlpatterns += [
    path('api/test/', get_data)
]

urlpatterns += [
    # Note; since we're not using Angular at the moment,
    # so this is going to use the /flags url. Otherwise we would use /api/flags
    path('users/', include('user_management.urls')),
    path('flags/', include('flag_scoring.urls')),
    path('teams/', include('team_management.urls')),
    path('scores/', include('score_view.urls')),
    path('services/', include('uptime_scoring.urls'))
]


urlpatterns += [
    # ... the rest of your URLconf goes here ...
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# Moving away from Angular for the time being for a number of reasons.
# Consult README.
# urlpatterns += [
#     # catch-all pattern for compatibility with Angular routes.
#     # This must be last in the list.
#     path('', views.index),
#     re_path(r'^(?P<path>.*)/$', views.index)
# ]