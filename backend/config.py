# FTP
import uptime_scoring

services = {
    'FTP': { # String that identifies the Service
        'SERVICE_NAME': 'FTP', # String that will show up in ScoringEvents
        'defaults': { # Default settings
            'ip': '192.168.43.47',
            'username': 'ftp',
            'password': 'ftp',
            'port': '22'
        },
        # 'thread_sleep': 2 * 60, # Two minutes, # Amount of time to wait before a service is checked again
        'thread_sleep': 60, # Do not change before dry running
        'recovery_multiplier': 2, # Multiplier that dictates how many points will be returned if the service is brought back up
        'point_value': 10, # Amount of points to be deducted when a service is down
        'default_change_request_limit': 5, # Amount of times passwords and settings can be changed
        'monitor': True, # Whether or not this service should be monitored.
        'thread_function': 'uptime_scoring.ftp_scoring.monitor_ftp'  # Function to call to start monitoring this service
    },
    'Wordpress': {
        'SERVICE_NAME': 'Company Blog',
        'defaults': {
            'ip': '192.168.43.151',
            'username': 'admin',
            'password': 'password',
        },
        'default_change_request_limit': 3,
        'recovery_multiplier': 15,
        #'thread_sleep': 2 * 60,  # Two minutes,
        'thread_sleep': 60,  # 10 seconds,
        'point_value': 10,
        'monitor': True,
        'thread_function': 'uptime_scoring.wordpress_scoring.monitor_wordpress'
    },
    'BigApp': {
        'SERVICE_NAME': 'BigApp',
        'defaults': {
            'ip': '192.168.43.74',
            'username': 'admin@admin.com',
            'password': 'password',
        },
        'default_change_request_limit': 3,
        'recovery_multiplier': 15,
        'thread_sleep': 60,
        #'thread_sleep': 10,  # 10 seconds,
        'point_value': 10,
        'monitor': True,
        'thread_function': 'uptime_scoring.bigapp_scoring.monitor_bigapp'
    }
}


# Team Settings
blue_team_starting_score = 1200

ratelimit_rate = '50/s'
ratelimit_key = 'ip'