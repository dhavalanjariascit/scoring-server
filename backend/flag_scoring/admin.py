from django.contrib import admin
from .models import Flag

# Register your models here.
class FlagAdmin(admin.ModelAdmin):
    pass


admin.site.register(Flag, FlagAdmin)