from django.apps import AppConfig


class FlagScoringConfig(AppConfig):
    name = 'flag_scoring'
