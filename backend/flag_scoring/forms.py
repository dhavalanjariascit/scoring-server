from django import forms
from .models import Flag


class SubmitFlagForm(forms.Form):
    flag_string = forms.CharField(max_length=200, required=True)


class FlagForm(forms.ModelForm):

    class Meta:
        model = Flag
        fields = ('flag_id', 'flag_string', 'description', 'value')

