from django.db import models
from team_management.models import Team, TeamMember


class Flag(models.Model):
    """Base class for flags.

    Contains basic attributes that could be extended for different situations later.
    """
    flag_id = models.AutoField(primary_key=True)
    flag_string = models.CharField(max_length=256, blank=False, unique=True)
    value = models.IntegerField(null=False)
    description = models.CharField(max_length=200, blank=False)


class SubmittedFlag(models.Model):
    flag = models.ForeignKey(Flag, on_delete=models.CASCADE)
    team_member = models.ForeignKey(TeamMember, on_delete=models.CASCADE, default=0)
    # submitted_by = models.ForeignKey(Team)