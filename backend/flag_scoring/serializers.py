from .models import Flag
from rest_framework.serializers import ModelSerializer

class FlagSerializer(ModelSerializer):
    class Meta:
        model = Flag
        fields = ('flag_id', 'description', 'flag_string', 'value')
