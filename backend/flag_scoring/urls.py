from django.urls import path
from . import views

urlpatterns = [
    path('', views.all_flags, name='all-flags'),
    path('<int:flag_id>', views.get_flags, name='view-flag'),
    path('add/', views.add_flag, name='add-flag'),
    path('delete/<int:flag_id>', views.delete_flag, name='delete-flag'),
    path('edit/<int:flag_id>', views.edit_flag, name='edit-flag'),
    # path('update/<int:flag_id>', views.update_flag),
    # path('verify/<int:flag_id>/<flag_string>', views.verify_flag,
    path('submit', views.submit_flag, name='submit-flag')
]

urlpatterns += [
]