from django.contrib.auth.decorators import login_required, permission_required
from django.http import Http404
from django.shortcuts import render, HttpResponse, redirect
from .models import Flag, SubmittedFlag
from score_view.models import ScoringEvent
from .serializers import FlagSerializer
from django.shortcuts import render
from ratelimit.decorators import ratelimit
import config
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .forms import SubmitFlagForm, FlagForm
import logging
log = logging.getLogger('app')


@ratelimit(key=config.ratelimit_key, rate=config.ratelimit_rate, block=True)
@login_required()
def submit_flag(request):
    """
    Submit a flag and then invalidate it.
    :param request:
    :return:
    """

    if request.method == "POST":
        form = SubmitFlagForm(request.POST)
        user = request.user
        try:
            team = user.teammember.team
        except Exception as ex:
            log.debug("Error: " + user.username + " does not have a team. ")
            generic_error = "Error: " + user.username + " does not have a team. "
            return render(request, 'flags.html', {'generic_error': generic_error})
        if form.is_valid() and team is not None:
            flag_string = form.cleaned_data.get('flag_string').lstrip().rstrip()
            flag = Flag.objects.filter(flag_string=flag_string).first()

            if flag is None:
                form.errors['flag_string'] = "Invalid flag string. This flag does not exist."
                return render(request, 'flags.html', {'form': form})

            existing_flag = SubmittedFlag.objects.filter(flag__flag_string=flag_string).first()
            if existing_flag is None:

                new_submitted_flag = SubmittedFlag(flag=flag, team_member=user.teammember)
                new_submitted_flag.save()

                # Give the team the score.
                new_scoring_event = ScoringEvent()
                # TODO: use a namedtuple for EVENT_TYPES
                new_scoring_event.event_type = ScoringEvent.EVENT_TYPES[0][0]
                new_scoring_event.description = "Team " + team.team_name + " found flag: " + flag.description
                new_scoring_event.point_value = flag.value
                new_scoring_event.team = team
                new_scoring_event.save()
                team.save()
                return redirect('scoring-event-list')
            else:
                form.errors['flag_string'] = "Flag string has already been submitted"
    else:
        form = SubmitFlagForm()
        flags = [flag for flag in Flag.objects.all()]
    return render(request, 'flags.html', {'form': form, 'flags': flags})


def all_flags(request):
    flags = [flag for flag in Flag.objects.all()]
    return render(request, 'flags.html', {'flags': flags})


@ratelimit(key=config.ratelimit_key, rate=config.ratelimit_rate, block=True)
@api_view(['GET'])
def get_flags(request, flag_id=0):
    """
    List all flags
    :param request:
    :return:
    """
    if request.method == "GET":
        if flag_id == 0:
            serializer = FlagSerializer(data=None)
            flags = Flag.objects.all()
            serializer = FlagSerializer(flags, many=True)
            return Response(serializer.data)
        else:
            flag = Flag.objects.filter(flag_id=flag_id).first()
            if flag is not None:
                serializer = FlagSerializer(flag, many=False)
                return Response(serializer.data)
            else:
                return Http404


@login_required
@permission_required('scoring_server.can_do_admin')
def add_flag(request):

    context = {}
    if request.method == "GET":
        form = FlagForm()
        context['form'] = form

    if request.method == "POST":
        form = FlagForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('scoring-event-list')
        else:
            context['form'] = form
    return render(request, 'add-flag.html', context)


@login_required
@permission_required('scoring_server.can_do_admin')
def edit_flag(request, flag_id):
    flag = Flag.objects.filter(flag_id=flag_id).first()
    context = {}
    form = FlagForm()
    if request.method == "POST":
        form = FlagForm(instance=flag, data=request.POST)
        if form.is_valid():

            form.save()
            return redirect('all-flags')

    context['form'] = form
    context['flag'] = flag

    return render(request, 'edit_flag.html', context)


@api_view(['GET'])
def verify_flag(request, flag_id, flag_string):
    """
    Verify that the flag string matches the flag id.

    :param request:
    :param flag_id: The ID of the flag.
    :param flag_string: The flag string supplied by the user.
    :return:
    """
    flag = Flag.objects.filter(flag_id=flag_id).first()

    if flag_string == flag.flag_string:
        return Response({'valid': True})
    else:
        return Response({'valid': False})


def delete_flag(request, flag_id):
    flag = Flag.objects.filter(flag_id=flag_id).first()
    if flag is not None:
        flag.delete()

    return redirect('all-flags')