from django.apps import AppConfig


class ScoreViewConfig(AppConfig):
    name = 'score_view'
