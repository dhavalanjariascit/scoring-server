from django.db import models
from team_management.models import Team

class ScoringEvent(models.Model):
    """
    Represents a scoring event.

    A scoring event is something that either increases a team's score. This
    """
    event_id = models.AutoField(primary_key=True)
    timestamp = models.DateTimeField(auto_now=True)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    # TODO: use a namedtuple
    EVENT_TYPES = [
        ('1', "FLAG_FOUND"),
        ('2', "SERVICE_DOWN"),
        ('3', "DATA_LEAK"),
        ('4', "OTHER")
    ]

    event_type = models.CharField(max_length=2, choices=EVENT_TYPES, blank=False)
    description = models.CharField(max_length=200, blank=False, null=False)
    point_value = models.IntegerField(default=0, null=False)