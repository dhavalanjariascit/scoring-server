from django.urls import path
from .views import ScoringEventListView

urlpatterns = [
    path('', ScoringEventListView.as_view(), name='scoring-event-list')
]