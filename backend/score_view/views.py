from .models import *
from django.views.generic.list import ListView
from ratelimit.mixins import RatelimitMixin
import config
from uptime_scoring.tools import monitor_all_services
import logging
log = logging.getLogger('app')



class ScoringEventListView(ListView, RatelimitMixin):
    # Since this is the main start page, background threads like the service monitors
    # should start here.

    def __init__(self):
        super(ScoringEventListView, self).__init__()
        monitor_all_services()

    ratelimit_key = config.ratelimit_key
    ratelimit_rate = config.ratelimit_rate

    ratelimit_block = True
    ratelimit_method = 'ALL'
    model = ScoringEvent
    ordering = ['-timestamp']
    template_name = 'scoring-event-list.html'


