from django import forms
from django.contrib.auth.hashers import make_password, check_password
from django.contrib.auth.models import User
from .models import Team


class TeamForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = ('team_name', 'team_password', 'description', 'team_type')

    def save(self, commit=True):
        instance = super(TeamForm, self).save(commit=False)
        instance.team_password = make_password(
            instance.team_password , salt=None, hasher='pbkdf2_sha256')
        if commit:
            instance.save()
        return instance


# The reason there are two forms is so that team details can be
# changed without changing the password
class UpdateTeamForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = ('team_name', 'description')


class ChangeTeamPasswordForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = ('team_password', )

    def save(self, commit=True):
        instance = super(ChangeTeamPasswordForm, self).save(commit=False)
        instance.team_password = make_password(
            instance.team_password, salt=None, hasher='pbkdf2_sha256')
        if commit:
            instance.save()
        return instance



class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')


class TeamMemberForm(forms.Form):
    team_id = forms.IntegerField()
    username = forms.CharField(max_length=200, required=True)
    email = forms.EmailField(required=True)
    password = forms.CharField(max_length=200, required=True)
    team_password = forms.CharField(max_length=200, required=True)

    def clean(self):
        cleaned_data = super(TeamMemberForm, self).clean()
        team_password = cleaned_data.get('team_password')

        team = Team.objects.filter(team_id=cleaned_data.get('team_id')).first()
        if team is not None and not check_password(team_password, team.team_password):
            self.add_error('team_password', "Team password is wrong.")

        return cleaned_data


class ChangeUserPaswordForm(forms.Form):
    username = forms.CharField(max_length=200, required=True)
    password = forms.CharField(max_length=200, required=True)
    confirm_password = forms.CharField(max_length=200, required=True)

    def clean(self):
        cleaned_data = super(ChangeUserPaswordForm, self).clean()
        if cleaned_data['password'] != cleaned_data['confirm_password']:
            self.add_error('password', "Passwords must match")

        return cleaned_data




