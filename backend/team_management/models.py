from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Team(models.Model):
    """
    Represents one team.

    """
    team_id = models.AutoField(primary_key=True)
    team_name = models.CharField(max_length=200, null=False, blank=False, unique=True)

    current_score = models.IntegerField(default=0)
    description = models.CharField(max_length=256)
    max_size = models.IntegerField(default=4)
    # TODO: Hash this?
    team_password = models.CharField(max_length=256, default="")

    TEAM_TYPE = [
        ('RT', 'RED_TEAM'),
        ('BT', 'BLUE_TEAM')
    ]
    team_type = models.CharField(max_length=3, choices=TEAM_TYPE)

    def __str__(self):
        return self.team_name


class TeamMember(models.Model):
    """
    Individual team member.

    Also, this is a custom user table that has the following fields rather the fields provided by Django
    out of the box.
    """
    # Add a OneToOne field so it extends the UserModel without much changes.
    # We will probably use a custom UserCreationForm to abstract this away
    id = models.AutoField(primary_key=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE, default=1)
    member_name = models.CharField(max_length=200, null=False, blank=False, unique=True)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.email

# This makes sure that the TeamMember model will automatically be created when
# we create a user model.


# @receiver(post_save, sender=User)
# def create_team_member(sender, instance, created, **kwargs):
#     if created:
#         TeamMember.objects.create(user=instance)

# @receiver(post_save, sender=User)
# def save_team_member(sender, instance, **kwargs):
#     instance.team.save()


