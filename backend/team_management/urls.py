from django.urls import path
from .views import *

urlpatterns = [

    path('', all_teams, name='all-teams'),
    path('add-team', add_team, name='add-team'),
    path('add-team-member/<int:team_id>', add_team_member, name='add-team-member'),
    path('edit/<int:team_id>', edit_team, name='edit-team'),
    path('delete/<int:team_id>', delete_team, name='delete-team'),
    path('change-team-password/<int:team_id>', change_team_password, name='change-team-password'),
    path('change-user-password', change_user_password, name='change-user-password')
]