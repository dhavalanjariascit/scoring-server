from django.contrib.auth.hashers import make_password
from django.shortcuts import render, redirect
from .forms import TeamMemberForm, TeamForm, ChangeUserPaswordForm, UpdateTeamForm, ChangeTeamPasswordForm
from django.contrib.auth.models import User
from .models import Team, TeamMember
from score_view.models import ScoringEvent
import config
from django.contrib.auth.decorators import permission_required, login_required
import logging
log = logging.getLogger('app')


def all_teams(request):
    context = {}
    context['teams'] = [t for t in Team.objects.all()]
    context['team_members'] = {}
    for team in context['teams']:
        context['team_members'][team.team_name] = []
        for team_member in team.teammember_set.all():
            context['team_members'][team.team_name].append(team_member.member_name)
        if team.team_type == 'BT':
            current_team_score = config.blue_team_starting_score + \
                                 sum([ev.point_value for ev in ScoringEvent.objects.filter(team=team)]) \
                                 - sum([ev.point_value for ev in ScoringEvent.objects.filter(team__team_type='RT')])
        else:
            current_team_score = sum([ev.point_value for ev in ScoringEvent.objects.filter(team=team)])
        team.current_score = current_team_score

    return render(request, 'all_teams.html', context)


@login_required()
@permission_required('scoring_server.can_do_admin')
def add_team(request):
    context = {}
    if request.method == "POST":
        team_form = TeamForm(request.POST)
        if team_form.is_valid():
            team_form.save()
            redirect('index')
        context['form'] = team_form
    else:
        form = TeamForm()
        context['form'] = form
    return render(request, 'add_team.html', context)


@login_required()
@permission_required('scoring_server.can_do_admin')
def add_team_member(request, team_id):

    if request.method == "GET":
        context = {}
        team = Team.objects.filter(team_id=team_id).first()
        if team is None:
            context['current_team'] = "Team not found."
        else:
            context['current_team'] = team.team_name
            context['team_id'] = team.team_id

    if request.method == "POST":
        context = {}
        team_member_form = TeamMemberForm(request.POST)
        team = Team.objects.filter(team_id=team_id).first()

        if team_member_form.is_valid():
            new_user = User()
            new_user.username = team_member_form.cleaned_data['username']
            new_user.email = team_member_form.cleaned_data['email']
            # TODO: hash
            new_user.password = make_password(
                team_member_form.cleaned_data['password'], salt=None, hasher='pbkdf2_sha256')
            # Save new user
            if User.objects.filter(username=new_user.username).first() != None:
                team_member_form.add_error("username", "Username already exists")
                context['form'] = team_member_form
                return render(request, 'add_team_member.html', context)

            new_user.save()
            new_team_member = TeamMember()
            new_team_member.user = new_user
            new_team_member.member_name = new_user.username
            new_team_member.team = team
            new_user.teammember = new_team_member

            new_team_member.save()
            try:
                new_team_member.save()
            except Exception as ex:
                log.debug("Error create new team member.")
            return redirect('all-teams')
        else:
            context['current_team'] = team.team_name
            context['team_id'] = team.team_id

        context['form'] = team_member_form

    return render(request, 'add_team_member.html', context)


@login_required()
@permission_required('scoring_server.can_do_admin')
def change_user_password(request):
    context = {}
    form = ChangeUserPaswordForm()
    if request.method == "POST":
        form = ChangeUserPaswordForm(request.POST)
        if form.is_valid():
            user = User.objects.filter(
                username=form.cleaned_data.get('username')).first()
            if user is not None:
                user.password = make_password(form.cleaned_data.get('password'))
                user.save()
                redirect('all-teams')
            else:
                form['errors']['username'] = "Username not found."

    context['form'] = form
    return render(request, 'change_user_password.html', context)


@login_required
@permission_required('scoring_server.can_do_admin')
def edit_team(request, team_id):
    context = {}
    team = Team.objects.filter(team_id=team_id).first()
    form = UpdateTeamForm()
    context['team'] = team
    if request.method == "POST":
        form = UpdateTeamForm(instance=team, data=request.POST)
        if form.is_valid():
            form.save()
            return redirect('all-teams')
        context['form'] = form

    return render(request, 'edit_team.html', context)



@login_required
@permission_required('scoring_server.can_do_admin')
def change_team_password(request, team_id):
    context = {}
    team = Team.objects.filter(team_id=team_id).first()
    form = ChangeTeamPasswordForm()
    context['team'] = team
    if request.method == "POST":
        form = ChangeTeamPasswordForm(instance=team, data=request.POST)
        if form.is_valid():
            form.save()
            return redirect('all-teams')
        context['form'] = form

    return render(request, 'change_team_password.html', context)



@login_required
@permission_required('scoring_server.can_do_admin')
def delete_team(request, team_id):

    team = Team.objects.filter(team_id=team_id).first()

    if team is not None:
        team.delete()

    return redirect('all-teams')
