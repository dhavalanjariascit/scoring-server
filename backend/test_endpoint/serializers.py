from rest_framework import serializers
from test_endpoint.models import TestModel

class TestModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestModel
        fields = ('firstname', 'lastname')

