from django.shortcuts import render, HttpResponseRedirect
from django.http import HttpResponse, JsonResponse
from test_endpoint.models import TestModel
from test_endpoint.serializers import TestModelSerializer
from django.views.decorators.csrf import csrf_exempt
import json

@csrf_exempt
def get_data(request):
    data = TestModel.objects.all()
    if request.method == "GET":
        test_data = {'test': 'OK'}
        return JsonResponse(test_data, safe=False)

