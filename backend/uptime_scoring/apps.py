from django.apps import AppConfig


class UptimeScoringConfig(AppConfig):
    name = 'uptime_scoring'
