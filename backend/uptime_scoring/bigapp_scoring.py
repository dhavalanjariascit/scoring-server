from score_view.models import ScoringEvent
from team_management.models import Team
from .tools import give_points_back_to_blue_team, was_service_down, create_service_down_scoring_event
from .views import *
import config
import time
import threading
import time
import requests
from requests.exceptions import ConnectionError, ConnectTimeout
from datetime import datetime
import logging
log = logging.getLogger('app')


service_string = 'BigApp'

def __bigapp_thread_worker():
    wp_settings = None
    settings_config = config.services[service_string]
    service_name = settings_config['SERVICE_NAME']
    defaults = settings_config['defaults']
    while True:
        username = defaults['username']
        password = ServiceSettings.objects.filter(service__service_name=service_name,
                                                  key='password').first().value
        login = 'http://' + defaults['ip'] + '/index.php'
        response = None
        if password is not None:
            try:
                with requests.Session() as s:
                    data = {
                        'email': username,
                        'password': password
                    }
                    response = s.post(login, data=data)
                    if '<title>Trades' not in str(response.content):
                        raise ConnectionError('invalid password')
                    else:
                        log.debug(service_name + " seems up, checked at: " + str(datetime.now()))

                if was_service_down(service_string):

                    log.debug(service_name + " was down, checked at: " + str(datetime.now()))
                    try:
                        give_points_back_to_blue_team(
                            config.services[service_string]['recovery_multiplier'],
                            config.services[service_string]['SERVICE_NAME']
                        )
                    except Exception:
                        log.debug("Error: Adding points to blue team failed.")
                wp_running = True
            except (ConnectionError, ConnectTimeout) as ex:

                log.debug(service_name + " is down at " + str(datetime.now()))
                log.debug("Exception: " + str(ex))
                create_service_down_scoring_event(service_string)
            except Exception as ex:
                log.debug("An unknown exception occured: " + str(ex))

        time.sleep(settings_config['thread_sleep'])


def monitor_bigapp(thread_name=''):
    try:
        if config.services[service_string]['monitor']:
            wp_thread = threading.Thread(target=__bigapp_thread_worker, name=thread_name)
            wp_thread.start()
            service = Services.objects.filter(
                service_name=service_string).first()
            service.is_monitored = True
            service.save()
    except Exception as ex:
        log.debug(str(ex))
