from django import forms

#
#
class FTPSettingsForm(forms.Form):
    current_ftp_port = forms.IntegerField()
    current_ftp_password = forms.CharField(max_length=200)


class WPSettingsForm(forms.Form):
    password = forms.CharField(max_length=200)


class BigAppSettingsForm(forms.Form):
    password = forms.CharField(max_length=200)