import config
from score_view.models import ScoringEvent
from uptime_scoring.models import ServiceSettings, Services
from team_management.models import Team
from .tools import give_points_back_to_blue_team, was_service_down, create_service_down_scoring_event
import time
from ftplib import FTP
import threading
from datetime import datetime

import logging
log = logging.getLogger('app')


def __ftp_thread_worker():
    ftp_settings = None
    settings_config = config.services['FTP']
    service_name = 'FTP'
    defaults = settings_config['defaults']
    while True:
        # We're not using the DB value because we don't want this to change on the fly
        ip = config.services['FTP']['defaults']['ip']
        username = config.services['FTP']['defaults']['username']
        password = ServiceSettings.objects.filter(service__service_name=service_name,
                                                  key='password').first().value
        if password is not None:
            try:
                ftp = FTP(ip)
                ftp.login(user=username,
                          passwd=password)

                result = ftp.sendcmd('pwd')
                log.debug("FTP result: " + str(result))
                ftp.close()
                log.debug(service_name + " seems up, checked at: " + str(datetime.now()))
                if was_service_down('FTP'):
                    log.debug("FTP server was down at" + str(datetime.now()))
                    try:
                        give_points_back_to_blue_team(
                            config.services['FTP']['recovery_multiplier'],
                            config.services['FTP']['SERVICE_NAME'])
                    except Exception:
                        log.debug("Error: Adding points to blue team failed.")

            except Exception as ex:  # TODO add a specific exception
                log.debug('FTP server exception: ' + str(ex))
                if "Errno" in str(ex) or 'WinError' in str(ex):
                    log.debug(service_name + " is down at " + str(datetime.now()))
                    create_service_down_scoring_event('FTP')
                else:
                    log.debug("Unknown exception FTP server: " + str(ex))

        time.sleep(settings_config['thread_sleep'])


def monitor_ftp(thread_name=''):
    try:
        if config.services['FTP']['monitor']:
            ftp_thread = threading.Thread(target=__ftp_thread_worker, name=thread_name)
            ftp_thread.start()
            service = Services.objects.filter(
                service_name='FTP').first()
            service.is_monitored = True
            service.save()

    except Exception as ex:
        log.debug(str(ex))


