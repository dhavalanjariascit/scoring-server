from django.db import models
import config

class UptimeLog(models.Model):
    """
    This model contains the log of all the times uptime was checked for a service
    """
    id = models.AutoField(primary_key=True)
    service_name = models.CharField(max_length=200, unique=True)
    point_value = models.IntegerField() # Default = 10. Not hardcoding here.
    timestamp = models.DateTimeField()


class Services(models.Model):
    """
    Model representing a service that we will monitor.
    """
    id = models.AutoField(primary_key=True)
    service_name = models.CharField(max_length=200, unique=True)
    change_requests_remaining = models.IntegerField(default=1)
    is_monitored = models.BooleanField(default=False)


class ServiceSettings(models.Model):
    """
    Generic service settings class that will contain changeable settings for various services.
    """
    id = models.AutoField(primary_key=True)
    service = models.ForeignKey(Services, on_delete=models.CASCADE)
    key = models.CharField(max_length=200)
    value = models.CharField(max_length=200)
