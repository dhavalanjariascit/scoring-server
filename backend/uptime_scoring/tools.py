import threading

from score_view.models import ScoringEvent
from team_management.models import Team
from .models import *
from datetime import datetime
import importlib
import logging
import math
log = logging.getLogger("app")


def give_points_back_to_blue_team(recovery_multiplier, service_string):
    """
    Give the blue team back scores if a service comes back up.
    However, the longer a service stays down, the fewer points will be
    recovered. The amount recovered also depends on the kind of service.
    So if the FTP server is brought back up it gives you more points
    but the WordPress (for example) will give them back fewer points.

    Note that this takes into account all the times the service was down,
    not us
    :return:
    """

    blue_team = Team.objects.filter(team_type='BT').first()
    total_loss = sum([ev.point_value for ev in ScoringEvent.objects.filter(
        team=blue_team, description__contains=service_string)]) * -1
    amount_of_service_down = len([ev.point_value for ev in ScoringEvent.objects.filter(
        team=blue_team, description__contains=service_string)])

    points_to_return = total_loss - (math.log(total_loss) * amount_of_service_down)

    recovery_scoring_event = ScoringEvent()
    recovery_scoring_event.team = blue_team
    recovery_scoring_event.description = service_string + " is back up at " + str(datetime.now())
    # Give half the points back.
    recovery_scoring_event.point_value = points_to_return
    recovery_scoring_event.save()


def was_service_down(service_string):
    """
    Return True if the last status showed that the service
    containing service_string was not running.
    :return:
    """
    service_name = config.services[service_string]["SERVICE_NAME"]
    ev = ScoringEvent.objects.filter(
        description__contains=service_name).order_by(
        '-timestamp').first()

    if ev is not None and 'down' in ev.description:
        return True
    else:
        return False


def initialize_services():
    for key, value in config.services.items():
        try:
            service, created = Services.objects.get_or_create(
                service_name=key)
            if created:
                log.debug('Service ' + value['SERVICE_NAME'] + " created")
            service.change_requests_remaining = config.services[key][
                'default_change_request_limit']
            service.save()
            for defaults_key, defaults_value in value['defaults'].items():
                setting, setting_created = ServiceSettings.objects.get_or_create(
                    service=service,
                    key=defaults_key,
                    value=defaults_value
                )
                if setting_created:
                    log.debug('Setting ' + defaults_key + " = " +
                              defaults_value + " created for " + service.service_name)
        except Exception as ex:
            # The reason the above code is put into try catch is that when migrating,
            # the above code fails because the tables required have not been created yet
            # making migrations fail. Since they fail, they can never be created.
            log.debug(str(ex))


def create_service_down_scoring_event(service):
    log.debug("Creating a service down scoring event for " + service)
    scoring_event = ScoringEvent()
    blue_team = Team.objects.filter(team_type='BT').first()
    scoring_event.team = blue_team
    scoring_event.description = config.services[service]['SERVICE_NAME'] + " is down at " + str(datetime.now())
    point_value = config.services[service]['point_value']
    scoring_event.point_value = scoring_event.point_value - point_value
    scoring_event.save()


def monitor_all_services():
    threads = {}
    for key, value in config.services.items():
        # Get the thread that has the name monitor_<service>
        monitor_service(key)



def monitor_service(service):

    thread_name = 'monitor_' + service
    existing_thread = [t for t in threading.enumerate() if t.name == thread_name]
    thread_exists = False
    thread_alive = False
    if len(existing_thread) > 0:
        existing_thread = existing_thread[0]
        thread_exists = True
        if existing_thread.is_alive():
            thread_alive = True
            service_obj = Services.objects.filter(service_name=service).first()
            service_obj.is_monitored = True
            service_obj.save()
    else:
        thread_exists = False
    if not thread_exists or not thread_alive:
        try:
            # Import the package using the namespace string and call the proper function
            package, module = config.services[service]['thread_function'].rsplit('.', 1)
            package = importlib.import_module(package)
            thread_function = getattr(package, module)
            thread_function(thread_name)
        except Exception as ex:
            log.debug("Error importing " +  config.services[service]['thread_function'])
            log.debug(ex)

    if not thread_alive:
        # If for whatever reason, the thread terminates.
        service_obj = Services.objects.filter(service_name=service).first()
        service_obj.is_monitored = False
        service_obj.save()


