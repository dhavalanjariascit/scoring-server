from django.urls import path
from . import views

urlpatterns = [
    path('', views.all_services, name='all-services'),
    # path('ftp', views.view_ftp, name='view-ftp'),
    path('ftp/edit', views.edit_ftp, name='edit-ftp'),
    path('wordpress/edit', views.edit_wordpress, name='edit-wordpress'),
    path('bigapp/edit', views.edit_bigapp, name='edit-bigapp'),
    path('monitor/<service>', views.monitor_service, name='monitor-service')
]

