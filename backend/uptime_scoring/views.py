from django.shortcuts import render, redirect
from django.contrib.auth.decorators import permission_required, login_required
from uptime_scoring import tools
from .models import ServiceSettings, Services
from .forms import FTPSettingsForm, WPSettingsForm, BigAppSettingsForm
import config
import  logging
log = logging.getLogger('app')


@login_required()
@permission_required('scoring_server.can_do_admin')
def edit_ftp(request):
    context = {}
    form = FTPSettingsForm()
    ftp_config_query = ServiceSettings.objects.filter(
        service__service_name='FTP')
    service = Services.objects.filter(service_name='FTP').first()
    if request.method == "POST":
        form = FTPSettingsForm(request.POST)
        if form.is_valid():
            if service.change_requests_remaining <= 0:
                form.add_error('current_ftp_password', 'No more change requests remaining.')
            else:
                service.change_requests_remaining = service.change_requests_remaining - 1
                service.save()
                port = ftp_config_query.filter(key='port').first()
                port.value = form.cleaned_data['current_ftp_port']
                port.save()
                password = ftp_config_query.filter(key='password').first()
                password.value = form.cleaned_data['current_ftp_password']
                password.save()
                context['success'] = True

            context['form'] = form

    context['current_settings'] = {}
    context['current_settings']['password'] = ftp_config_query.filter(key='password').first().value
    context['current_settings']['port'] = ftp_config_query.filter(key='port').first().value
    context['change_requests_remaining'] = service.change_requests_remaining
    return render(request, 'edit_ftp.html', context)


@login_required()
@permission_required('scoring_server.can_do_admin')
def edit_wordpress(request):
    context = {}
    form = WPSettingsForm()
    wp_config_query = ServiceSettings.objects.filter(
        service__service_name='Wordpress')

    service = Services.objects.filter(service_name='FTP').first()
    if request.method == "POST":
        form = WPSettingsForm(request.POST)
        if form.is_valid():

            if service.change_requests_remaining <= 0:
                form.add_error('password', 'No more change requests remaining.')
            else:
                service.change_requests_remaining = service.change_requests_remaining - 1
                service.save()
                password = wp_config_query.filter(key='password').first()
                password.value = form.cleaned_data.get('password')
                password.save()
                context['success'] = True


            context['form'] = form


    context['current_settings'] = {}
    context['current_settings']['password'] = wp_config_query.filter(key='password').first().value
    context['change_requests_remaining'] = service.change_requests_remaining
    return render(request, 'edit_wordpress.html', context)



@login_required()
@permission_required('scoring_server.can_do_admin')
def edit_bigapp(request):
    context = {}
    form = BigAppSettingsForm()
    bigapp_query = ServiceSettings.objects.filter(
        service__service_name='BigApp')
    service = Services.objects.filter(service_name='BigApp').first()
    if request.method == "POST":
        form = BigAppSettingsForm(request.POST)
        if form.is_valid():
            if service.change_requests_remaining <= 0:
                form.add_error('password', 'No more change requests remaining.')
            else:
                service.change_requests_remaining = service.change_requests_remaining - 1
                service.save()
                password = bigapp_query.filter(key='password').first()
                password.value = form.cleaned_data.get('password')
                password.save()
                context['success'] = True


            context['form'] = form

    context['current_settings'] = {}
    context['current_settings']['password'] = bigapp_query.filter(key='password').first().value
    context['change_requests_remaining'] = service.change_requests_remaining

    return render(request, 'edit_wordpress.html', context)



@login_required()
@permission_required('scoring_server.can_do_admin')
def monitor_service(request, service):
    context = {}
    service = Services.objects.filter(service_name=service).first()
    if service is None:
        log.debug("Service " + service + " not found")
        context['error'] = "ERROR: Service not found"
        return render(request, 'all-services.html', context)
    else:
        tools.monitor_service(service.service_name)
        context['services'] = Services.objects.all()
        return render(request, 'all_services.html', context)


def all_services(request):
    context = {}
    context['services'] = Services.objects.all()
    return render(request, "all_services.html", context)
