from score_view.models import ScoringEvent
from team_management.models import Team
from .tools import give_points_back_to_blue_team, was_service_down, create_service_down_scoring_event
from .views import *
import config
import time
import threading
import time
import requests
from requests.exceptions import ConnectionError, ConnectTimeout
from datetime import datetime
import logging
log = logging.getLogger('app')


def __wp_thread_worker():
    wp_settings = None
    settings_config = config.services['Wordpress']
    service_name = 'Wordpress'
    defaults = settings_config['defaults']
    while True:
        username = defaults['username']
        password = ServiceSettings.objects.filter(service__service_name=service_name,
                                                  key='password').first().value
        wp_login = 'http://' + defaults['ip'] + '/wordpress/wp-login.php'
        wp_admin = 'http://' + defaults['ip'] + '/wordpress/wp-admin/'
        response = None
        if password is not None:
            try:
                with requests.Session() as s:
                    headers = {'Cookie': 'wordpress_test_cookie=WP Cookie Check'}
                    data = {'log': username, 'pwd': password,
                            'wp-submit': 'Log In', 'redirect_to': wp_admin, 'testcookie': 1}
                    response = s.post(wp_login, headers=headers, data=data)
                    if '<title>Dashboard' not in str(response.content):
                        raise ConnectionError('invalid password')
                    else:
                        log.debug(service_name + " seems up, checked at: " + str(datetime.now()))

                if was_service_down('Wordpress'):

                    log.debug(service_name + " was down, checked at: " + str(datetime.now()))
                    try:
                        give_points_back_to_blue_team(
                            config.services['Wordpress']['recovery_multiplier'],
                            config.services['Wordpress']['SERVICE_NAME']
                        )
                    except Exception:
                        log.debug("Error: Adding points to blue team failed.")
                wp_running = True
            except (ConnectionError, ConnectTimeout) as ex:

                log.debug(service_name + " is down at " + str(datetime.now()))
                log.debug("Exception: " + str(ex))
                create_service_down_scoring_event('Wordpress')
            except Exception as ex:
                log.debug("An unknown exception occured: " + str(ex))

        time.sleep(settings_config['thread_sleep'])


def monitor_wordpress(thread_name=''):
    try:
        if config.services['Wordpress']['monitor']:
            wp_thread = threading.Thread(target=__wp_thread_worker, name=thread_name)
            wp_thread.start()
            service = Services.objects.filter(
                service_name='Wordpress').first()
            service.is_monitored = True
            service.save()
    except Exception as ex:
        log.debug(str(ex))
