from django.conf import settings
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User


class SettingsBackend:
    """
    Authenticate against the settings ADMIN_LOGIN and ADMIN_PASSWORD.

    Use the login name and a hash of the password. For example:

    """
    def authenticate(self, request, username=None, password=None):
        existing_user = User.objects.filter(username=username).first()
        if existing_user is not None:
            login_valid = True
            pwd_valid = check_password(password, existing_user.password)
            if login_valid and pwd_valid:
                return existing_user
            return None
