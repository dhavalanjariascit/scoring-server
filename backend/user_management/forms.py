from django import forms


class CreateAdminForm(forms.Form):
    username = forms.CharField(max_length=200, required=True)
    email = forms.EmailField(required=True)
    password = forms.CharField(max_length=200, required=True)
    confirm_password = forms.CharField(max_length=200, required=True)

    def clean(self):
        cleaned_data = super(CreateAdminForm, self).clean()
        password = cleaned_data.get('password')
        confirm_password = cleaned_data.get('password')

        if password != confirm_password:
            self.add_error('confirm_password', "Passwords do not match")

        return cleaned_data
