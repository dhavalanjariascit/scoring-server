from django.db import models

class ProxyModelForAdminPermission(models.Model):
    class Meta:
        app_label = 'scoring_server'
        managed = False
        permissions = (
            ('can_do_admin', 'Can do admin'),
        )