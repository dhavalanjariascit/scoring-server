from django.urls import path
from django.contrib.auth import logout
from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('login/', login_user, name='login'),
    path('logout', logout_user, name='logout'),
    path('create-admin', create_admin, name='create-admin')
]