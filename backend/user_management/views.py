from django.contrib.auth.hashers import make_password
from django.shortcuts import render
from django.contrib.auth.models import User, Group, Permission
from django.shortcuts import redirect
from django.contrib.contenttypes.models import ContentType
from ratelimit.decorators import ratelimit

import config
from .forms import CreateAdminForm
from uptime_scoring.models import Services
from django.contrib.auth import logout, login, authenticate
from .models import ProxyModelForAdminPermission
from team_management.models import Team
import logging
log = logging.getLogger('app')


@ratelimit(key=config.ratelimit_key, rate=config.ratelimit_rate, block=True)
def create_admin(request):
    context = {}
    if request.method == "POST":
        form = CreateAdminForm(request.POST)
        context['form'] = form
        if form.is_valid():
            new_admin, created = User.objects.get_or_create(username=form.cleaned_data.get('username'))
            if not created:
                form.errors['username'] = "Username already exists."
                log.debug('Username already exists')
            else:
                new_admin.password = make_password(form.cleaned_data['password'], salt=None, hasher='pbkdf2_sha256')
                new_admin.save()
                log.debug('New user created: ' + str(new_admin))
                admin_group, created = Group.objects.get_or_create(name='admins')
                # Create Permission
                content_type = ContentType.objects.get_for_model(ProxyModelForAdminPermission, for_concrete_model=False)
                admin_perms, created = Permission.objects.get_or_create(
                    codename='can_do_admin',
                    name='Can do admin',
                    content_type=content_type
                )
                admin_group.permissions.set((admin_perms, ))
                new_admin.groups.add(admin_group)
                log.debug('Redirecting to index page')
                return redirect('index')
        else:
            log.debug('form.is_valid returned false')
            context['form'] = form
            return render(request, 'create_admin.html', context)
    if request.method == "GET":
        # We check this here again in case this is not the first time we
        # are adding admins
        admin_group = Group.objects.filter(name='admins').first()
        if admin_group is None:
            return render(request, 'create_admin.html', context)

        if request.user is not None:
            if request.user.has_perm('can_do_admin'):
                return render(request, 'create_admin.html', context)
        return redirect('login')


@ratelimit(key=config.ratelimit_key, rate=config.ratelimit_rate, block=True)
def login_user(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user, user.backend)
            next = request.GET.get('next')
            if next is not None:
                return redirect(next)
            else:
                return redirect('scoring-event-list')
        else:
            return render(request, 'login.html', {'form': {"errors": "username or password is invalid"}})
    else:
        # admin_group = Group.objects.filter(name='admins').first()
        # if admin_group is None:
        #     log.debug('admin_group is None')
        #     return redirect('create-admin')
        # elif admin_group.user_set.count() == 0:
        #     return redirect('create-admin')
        return render(request, 'login.html')


@ratelimit(key=config.ratelimit_key, rate=config.ratelimit_rate, block=True)
def index(request, path=''):
    """
    Returns the setup page if there are no admins in the admin group or the scoring page
    :param request:
    :param path:
    :return:
    """

    ### First Run Code
    admin_group = Group.objects.filter(name='admins').first()
    if admin_group is None:
        # If there is no one in the admin group, initialize everything and
        # start monitoring
        try:
            from uptime_scoring.tools import initialize_services
            initialize_services()
        except ImportError as ex:
            log.debug("uptime_scoring module not initalized at this time.")

        blue_team, created = Team.objects.get_or_create(team_name="BLUE TEAM",
                                                        team_type='BT')
        return redirect('create-admin')

    # Monitoring code.
    for key, value in config.services.items():
        service = config.services[key]
        # If config says monitor the service and the service
        # is not being monitored

        try:
            from uptime_scoring.tools import monitor_service
            monitor_service(key)
        except ImportError as ex:
            log.debug("Module uptime_scoring not imported at this time")

    return redirect('scoring-event-list')


def logout_user(request):
    logout(request)
    return redirect('login')
